#include <AccelStepper.h>

AccelStepper stepper(AccelStepper::DRIVER, 9, 8);

int pos = 2000;

void setup() {
  Serial.begin(9600);
  stepper.setMaxSpeed(1000);
  stepper.setAcceleration(1000);
}

void loop() {
  if (stepper.distanceToGo() == 0) {
    delay(500);
    pos = -pos;
    stepper.moveTo(pos);
    Serial.println(pos);
  }
  stepper.run();
}

#include <ChainableLED.h>

const int sensorPin = A2;
ChainableLED led(7, 8, 1);

void setup() {
  Serial.begin(9600);
  pinMode(sensorPin, INPUT);

}

int threshold = 1;
#define RANGE 20
int history[RANGE];
long toffset = 0;

void loop() {
  int v = analogRead(A1);
  //Serial.println(v);

  // Push list.
  for (int i = 1; i < RANGE; i++) {
    history[i] = history[i - 1];
  }
  history[0] = v;

  Serial.println(v - history[RANGE - 1]);

  if (v - history[RANGE - 1] > threshold && millis() - toffset > 500) {
    //Serial.println("breathe out");
    toffset = millis();
  }

  float t = (millis() - toffset) / 1000.0;
  float r = max(0, -pow(t * 2 - 1, 2) + 1);
  led.setColorRGB(0, (int)round(r * 255), 0, 0);

  delay(40);
}

#include "header.h"

// Compute distance using dx and dy.
float dist(float dx, float dy) {
  return sqrt(pow(dx, 2) + pow(dy, 2));
}

// Joystick analog sliders.
const int joystickX = A0;
const int joystickY = A1;
float jStep = 0.01;

// Both steppers.
StepperMotor stepper1 = StepperMotor{7, 6, 0.0, 0.0};
StepperMotor stepper2 = StepperMotor{9, 8, 1.0, 0.0};

// Start configuration.
int stepsPerRevolution = 2*6400;
float x = 0.5;
float y = 0.5;

float stepper1_d = 1.0;
float stepper2_d = 1.0;

void recomputeConfiguration() {
  stepper1.pos = 0;
  stepper1.target = 0;
  stepper2.pos = 0;
  stepper2.target = 0;

  // Compute initial extension of wires.
  stepper1_d = dist(stepper1.x - x, stepper1.y - y);
  stepper2_d = dist(stepper2.x - x, stepper2.y - y);
}

// Pulley/speed settings.
float pulley1_r = 0.055 / 2;
float pulley2_r = 0.055 / 2;
float step_a = 2 * PI / stepsPerRevolution;
float step1_d = step_a * pulley1_r; // distance per step
float step2_d = step_a * pulley2_r; // distance per step
float velocity = 0.05; // meters per second

// Pending operation.
Coord *operation = 0;

// End of the list.
Coord *last_operation = 0;

void setup() {
  recomputeConfiguration();

  Serial.begin(9600);
  Serial.println("Initial stepper cord extension:");
  Serial.println(stepper1_d);
  Serial.println(stepper2_d);

  pinMode(joystickX, INPUT);
  pinMode(joystickY, INPUT);
  pinMode(stepper1.stepPin, OUTPUT);
  pinMode(stepper1.directionPin, OUTPUT);
  pinMode(stepper2.stepPin, OUTPUT);
  pinMode(stepper2.directionPin, OUTPUT);
  
  // Create circle around (0, 0.4) of radius 0.2
  /*int n = 100;
  float cx = 0, cy = 0.4, r = 0.15;
  for (int i = 0; i < n; i++) {
    float a = (float)i / n * 2 * PI;
    queueCoord(cx + r * cos(a), cy + r * sin(a));
  }*/
}

void queueCoord(float x, float y) {
  Coord *coord = (Coord*)malloc(sizeof(Coord));
  coord->x = x;
  coord->y = y;
  if (!last_operation) {
    Coord *intermediate = (Coord*)malloc(sizeof(Coord));
    intermediate->next = coord;
    operation = intermediate;
  } else {
    last_operation->next = coord;
  }
  coord->next = 0;
  last_operation = coord;
}

int ioState = 0;
float ioX, ioY;
bool enableJoystick = false;

void loop() {
  switch (ioState) {
    case 0:
      Serial.println("Enter X coordinate:");
      ioState++;
      break;

    case 1:
      if (Serial.available()) {
        ioX = Serial.parseFloat();
        Serial.println(ioX);
        Serial.println("Enter Y coordinate:");
        ioState++;
      }
      break;

    case 2:
      if (Serial.available()) {
        ioY = Serial.parseFloat();
        Serial.println(ioY);
        Serial.println("Enter command:");
        ioState++;
      }
      break;

    case 3:
      if (Serial.available()) {
        switch (Serial.read()) {
          case 'm': // move to
            queueCoord(ioX, ioY);
            Serial.print("Added coordinate (");
            Serial.print(ioX);
            Serial.print("; ");
            Serial.print(ioY);
            Serial.println(")");
            break;
          
          case 's': // set coordinates of stepper2
            stepper2.x = ioX;
            stepper2.y = ioY;
            recomputeConfiguration();
            break;
          
          case 'p': // set coordinates of pointer
            x = ioX;
            y = ioY;
            recomputeConfiguration();
            break;
          
          case 'v': // set speed
            velocity = ioX;
            break;
          
          case 'j': // toggle joystick
            enableJoystick = !enableJoystick;
            break;
        }
        ioState = 0;
      }
      break;
  }
  
  // If current instruction is ready, move on to the next one.
  bool turn1 = runStepper(stepper1);
  bool turn2 = runStepper(stepper2);
  if (!turn1 && !turn2 && operation) {
    // free current operation.
    Coord *next = (Coord*)operation->next;
    free(operation);
    operation = operation->next;

    if (operation) {
      //Serial.println("executing new operation");

      // Compute time.
      float t = dist(operation->x - x, operation->y - y) / velocity; // time to complete operation

      // Compute desired length of wires.
      float dx1 = stepper1.x - operation->x;
      float dy1 = stepper1.y - operation->y;
      float dx2 = stepper2.x - operation->x;
      float dy2 = stepper2.y - operation->y;
      float d1 = dist(dx1, dy1);
      float d2 = dist(dx2, dy2);

      // Compute desired number of steps.
      long n_steps_1 = (long)round((d1 - stepper1_d) / step1_d);
      long n_steps_2 = (long)round((d2 - stepper2_d) / step2_d);
      stepper1.target = -n_steps_1;
      stepper2.target = n_steps_2;

      // Adjust speed.
      int rel1 = stepper1.target - stepper1.pos;
      int rel2 = stepper2.target - stepper2.pos;
      stepper1.v = abs((float)rel1 / t);
      stepper2.v = abs((float)rel2 / t);
      Serial.print(rel1);
      Serial.print(" -> ");
      Serial.print(stepper1.v);
      Serial.println(" steps per second");
      Serial.print(rel2);
      Serial.print(" -> ");
      Serial.print(stepper2.v);
      Serial.println(" steps per second");

      // update x and y coordinates (only used for time computation).
      x = operation->x;
      y = operation->y;
    } else {
      last_operation = 0;
      //Serial.println("halt");
    }
  } else if (!turn1 && !turn2) {
    float jx = analogRead(joystickX) / 1023.0 * 2.0 - 1.0;
    float jy = analogRead(joystickY) / 1023.0 * 2.0 - 1.0;
    if (enableJoystick && dist(jx, jy) > 0.4) {
      queueCoord(x + jx * jStep, y + -jy * jStep);
    }
  }
}

bool runStepper(StepperMotor &m) {
  if (m.pos != m.target) {
    unsigned long t = micros();
    int interval = (int)round((float)1000000 / m.v / 2);
    if (t - m.lastStep > (unsigned long)interval) {
      int d = m.target > m.pos ? 1 : -1;
      m.lastStep = t;
      m.pos += d;
      m.stepValue = m.stepValue == HIGH ? LOW : HIGH; // Alternate
      digitalWrite(m.directionPin, d == 1 ? HIGH : LOW);
      digitalWrite(m.stepPin, m.stepValue);
    } else if (t < m.lastStep) {
      Serial.println("time overflow");
      m.lastStep = t; // After about 70 minutes the micros value flows over.
    }
    return true;
  } else {
    return false;
  }
}

